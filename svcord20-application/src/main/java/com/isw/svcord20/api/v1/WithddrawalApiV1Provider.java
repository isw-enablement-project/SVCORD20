package com.isw.svcord20.api.v1;

import com.isw.svcord20.domain.svcord.service.WithdrawalDomainService;
import com.isw.svcord20.sdk.api.v1.api.WithddrawalApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import com.isw.svcord20.sdk.api.v1.model.ErrorSchema;
import com.isw.svcord20.sdk.api.v1.model.WithdrawalBodySchema;
import com.isw.svcord20.sdk.api.v1.model.WithdrawalResponseSchema;
import com.isw.svcord20.sdk.api.v1.model.WithdrawalResponseSchema.ServicingOrderWorkTaskResultEnum;
import com.isw.svcord20.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord20.sdk.domain.svcord.entity.WithdrawalDomainServiceInput;
import com.isw.svcord20.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord20.sdk.domain.svcord.type.ServicingOrderWorkResult;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.valueextraction.Unwrapping.Skip;


/**
 * A stub that provides implementation for the WithddrawalApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.isw.svcord20.sdk.api.v1.api")
public class WithddrawalApiV1Provider implements WithddrawalApiV1Delegate {

  @Autowired
  WithdrawalDomainService withdrawalDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;

  @Override
  public ResponseEntity<WithdrawalResponseSchema> servicingOrderWithdrawalPost(WithdrawalBodySchema withdrawalBodySchema) {
    //TODO Auto-generated method stub

    // WithdrawalResponseSchema withdrawalResponse = new WithdrawalResponseSchema();
    WithdrawalDomainServiceInput withdrawalDomainServiceInput = entityBuilder.getSvcord()
    .getWithdrawalDomainServiceInput()
    .setAccountNumber(withdrawalBodySchema.getAccountNumber())
    .setAmount(withdrawalBodySchema.getAmount())
    .build();

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = withdrawalDomainService.execute(withdrawalDomainServiceInput);

    if (withdrawalDomainServiceOutput == null) {
      ErrorSchema errorSchema = new ErrorSchema();
      errorSchema.setErrorId("500");
      errorSchema.setErrorMsg("INTERNAL ERROR");
      ResponseEntity.status(500).body(errorSchema);
    }

    WithdrawalResponseSchema withdrawalResponseSchema = new WithdrawalResponseSchema();
    withdrawalResponseSchema.setServicingOrderWorkTaskResult((ServicingOrderWorkTaskResultEnum.valueOf(withdrawalDomainServiceOutput.getServicingOrderWorkResult().toString())));
    withdrawalResponseSchema.setTransactionId(withdrawalDomainServiceOutput.getTrasactionId());

    return ResponseEntity.status(200).body(withdrawalResponseSchema);
  }

}
