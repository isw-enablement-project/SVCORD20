package com.isw.svcord20.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord20.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord20.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord20.sdk.domain.svcord.entity.Svcord20;
import com.isw.svcord20.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord20.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord20.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord20.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord20.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord20.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord20.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord20.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord20.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord20.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord20.sdk.integration.paymord.paymord.model.CusotmerReference;
import com.isw.svcord20.domain.svcord.command.Svcord20Command;
import com.isw.svcord20.integration.partylife.service.RetrieveLogin;
import com.isw.svcord20.integration.paymord.service.PaymentOrder;
import com.isw.svcord20.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord20.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  @Autowired
  Svcord20Command serviceOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord20.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord20.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic
    RetrieveLoginInput retrieveLoginInput = integrationEntityBuilder
    .getPartylife()
    .getRetrieveLoginInput().setId("test1")
    .build();

    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveLoginInput);

    if(retrieveLoginOutput.getResult() != "SUCCESS") {
      return null;
    }

    CustomerReferenceEntity customerReferenceEntity = this.entityBuilder
    .getSvcord()
    .getCustomerReference().build();

    customerReferenceEntity.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReferenceEntity.setAmount(withdrawalDomainServiceInput.getAmount());
  
    CreateServicingOrderProducerInput createServicingOrderProducerInput = this.entityBuilder.getSvcord()
    .getCreateServicingOrderProducerInput().build();
  
    // createInput.setCustomerReference(customerReference);
    // Svcord19 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);
  
    createServicingOrderProducerInput.setCustomerReference(customerReferenceEntity);
    Svcord20  createServicingOrderProducerOutput = serviceOrderProcedureCommand.createServicingOrderProducer(createServicingOrderProducerInput);

    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD20");
    paymentOrderInput.setExternalSerive("SVCORD20");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);


    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
    .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(createServicingOrderProducerOutput.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    serviceOrderProcedureCommand.updateServicingOrderProducer(createServicingOrderProducerOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord()
    .getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();
  
    return withdrawalDomainServiceOutput;
  }

}
