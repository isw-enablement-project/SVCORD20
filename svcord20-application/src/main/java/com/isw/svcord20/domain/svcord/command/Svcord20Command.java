package com.isw.svcord20.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord20.sdk.domain.svcord.command.Svcord20CommandBase;
import com.isw.svcord20.sdk.domain.svcord.entity.Svcord20;
import com.isw.svcord20.sdk.domain.svcord.entity.Svcord20Entity;
import com.isw.svcord20.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord20.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord20.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord20.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord20.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord20.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord20Command extends Svcord20CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord20Command.class);

  public Svcord20Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord20.sdk.domain.svcord.entity.Svcord20 createServicingOrderProducer(com.isw.svcord20.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord20Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic

      Svcord20Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord20().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);

      ThirdPartyReferenceEntity thirdPartyReferenceEntity = new ThirdPartyReferenceEntity();
      thirdPartyReferenceEntity.setId("test1");
      thirdPartyReferenceEntity.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReferenceEntity);
   
      // 도메인.ROOT_ENTITY.save(저장)
      return repo.getSvcord().getSvcord20().save(servicingOrderProcedure);
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord20.sdk.domain.svcord.entity.Svcord20 instance, com.isw.svcord20.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord20Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic

      Svcord20Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord20()
      .getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      // servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      // servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

      // log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      // log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      // log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());
    
      this.repo.getSvcord().getSvcord20().save(servicingOrderProcedure);  
    }
  
}
